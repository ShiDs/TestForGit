package controls;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by 99502 on 2017/7/13.
 */
@WebServlet(name = "HelloServlet")
public class HelloServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<title>Hello Servlet</title><body>");
        out.println("现在时间是：" + new java.util.Date());
        out.println("<br/>");
        out.println("啦啦啦啦");
        out.println("<br/>");
        out.println("哈哈哈哈");
        out.println("<br/>");
//        out.println("第四56789次提交");
//        out.println("2017-7-13 第2次提交");
        out.println("2017-7-13 测试push");
        out.println("</body>");
        out.println("</html>");
    }
}
